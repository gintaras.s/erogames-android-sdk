#!/bin/zsh
#
# version: 2.0.6
#
# "Whitelabeling" of the APK.
# Warning! The APK will be resigned.

declare -r OUTPUT_DIRECTORY="out"
declare -r WL_PROPERTIES=wl.properties
declare -r ANDROID_MANIFEST=AndroidManifest.xml

# Erogames Auth metadata
declare -r META_DATA_WL_ID="erogames_auth_whitelabel_id"
# Erogames Analytics metadata
declare -r META_DATA_ANLTCS_SOURCE_ID="erogames_analytics_source_id"
declare -r META_DATA_ANLTCS_WL_ID="erogames_analytics_whitelabel_id"
declare -r META_DATA_ANLTCS_WL_ACCESS_KEY="erogames_analytics_whitelabel_access_key"

declare -r DATETIME_FORMAT="%s"

COLOR_RED=$(tput setaf 1)
COLOR_GREEN=$(tput setaf 2)
STYLE_RESET=$(tput sgr0)
TEXT_BOLD=$(tput bold)
readonly COLOR_RED
readonly COLOR_GREEN
readonly STYLE_RESET
readonly TEXT_BOLD

COUNTER=0
FILES_COUNT=0

#######################################
# Сolored 'echo'
# Example 1:
#   echolog "some_string"
#   out: {prefix} some_string
# Example 2:
#   echolog "some_string" ""
#   out: some_string
# Example 3:
#   echolog "some_string" "$COLOR_RED"
#   out: {RED prefix} some_string
# Globals:
#   COLOR_GREEN, STYLE_RESET
# Arguments:
#    String to output.
#    (Option) Prefix color. Prefix will not be printed if this arg is empty.
#######################################
function echolog() {
  if [[ "$#" -gt 1 ]]; then
    if [ -z "${2}" ]; then
      prefix=""
      prefix_color="$STYLE_RESET"
    else
      prefix="==>"
      prefix_color="${2}"
    fi
  else
    prefix="==>"
    prefix_color="$COLOR_GREEN"
  fi
  echo "${prefix_color}${prefix}${STYLE_RESET} ${1}"
}

function get_args() {
  for arg in "$@"; do
    echo "$arg"
  done
}

#######################################
# Parse and return property value from *.properties file
# Globals:
#   WL_PROPERTIES
# Arguments:
#   Property key
# Returns:
#   Property value
#######################################
function get_property() {
  local prop_key="${1}"
  prop_value=$(cat "$WL_PROPERTIES" | grep "$prop_key" | cut -d'=' -f2)
  echo "$prop_value"
}

#######################################
# Create directory. If directory exists the suffix "(n)" will be added to name.
# Example. Input directory name: cats.
# If directory exists then new name would be "cats(0)"
# Arguments:
#   Directory name
# Returns:
#   Directory name
#######################################
function create_working_directory() {
  local working_directory="${1}"
  if [ -d "$working_directory" ]; then
    i=0
    while [[ -d "$working_directory($i)" ]]; do
      ((i++))
    done
    mkdir "$working_directory($i)"
    working_directory="$working_directory($i)"
  fi
  echo "$working_directory"
}

#######################################
# Align APK
# Arguments:
#   APK file path
#######################################
function zipalign_apk() {
  local apk="${1}"
  local zipalign
  zipalign="$(get_property "buildToolsPath")/zipalign"

  echolog "Align..."
  "$zipalign" -p -f 4 "$apk" "aligned_${apk}"
  echolog "Confirm the alignment..."
  "$zipalign" -c 4 "aligned_${apk}"
  rm "${apk}"
  mv "aligned_${apk}" "${apk}"
}

#######################################
# Sign APK
# Arguments:
#   APK file path
#######################################
function sign_apk() {
  local apk="${1}"
  local keystore_file
  local keystore_password
  local key_password
  local key_alias
  local apksigner

  keystore_file=$(get_property "keystoreFile")
  keystore_password=$(get_property "keystorePassword")
  key_password=$(get_property "keyPassword")
  key_alias=$(get_property "keyAlias")
  apksigner="$(get_property "buildToolsPath")/apksigner"

  echolog "Signing..."
  java -jar "$apksigner" sign --v4-signing-enabled false --ks "$keystore_file" --ks-key-alias "$key_alias" --ks-pass "pass:$keystore_password" --key-pass "pass:$key_password" "$apk"

  echolog "Signature verifying..."
  java -jar "$apksigner" verify "$apk"
}

#######################################
# Replace value in strings.xml
# Globals:
#   ANDROID_MANIFEST
# Arguments:
#   Target resource id
#   New value
#######################################
function replace_metadata_value() {
  local name="${1}"
  local value="${2}"

  echolog "Patch $name:"
  echolog "   new value: ${TEXT_BOLD}${value}${STYLE_RESET}" ""

  xmlstarlet edit --inplace \
    --update "//application/meta-data[@android:name=\"$name\"]/@android:value" \
    --value "$value" "$ANDROID_MANIFEST"
}

#######################################
# Run whitelabel resolving process.
# Unpack, patch, pack, sign.
# Globals:
#   WHITELABEL
#   META_DATA_WL_ID
#   OUTPUT_DIRECTORY
#   COUNTER
#   FILES_COUNT
# Arguments:
#   File path
#######################################
function resolve() {
  local file="${1}"
  echolog "Start patching..."
  echolog "APK: ${file}"
  echolog "Whitelabel: $WHITELABEL"

  working_directory=$(create_working_directory "$WHITELABEL")
  echolog "Working directory: $working_directory"

  # Unpack APK into working directory
  echolog "Unpucking..."
  apktool d --no-src "$file" -o "$working_directory" -f
  cd "$working_directory" || exit

  # Check if there are required string resources
  # If no required string resources in current APK then
  # APK will be renamed by adding "no_sdk" suffix.
  if ! grep -Eq "$META_DATA_WL_ID" "$ANDROID_MANIFEST"; then
    echolog "${COLOR_RED}Missing meta data:${STYLE_RESET}" "${COLOR_RED}"
    echolog "   ${TEXT_BOLD}${COLOR_RED}${META_DATA_WL_ID}${STYLE_RESET}" ""
    echolog "Remove \"$working_directory\" directory..."
    ((FILES_COUNT--))
    cd - || exit
    rm -r "$working_directory"
    mv "${file}" "${file}.no_sdk"
    return
  fi

  # Patch META_DATA_WL_ID
  replace_metadata_value "$META_DATA_WL_ID" "$WHITELABEL"

  # Patch META_DATA_ANLTCS_SOURCE_ID
  if [ -n "$ANLTCS_SOURCE_ID" ]; then
    replace_metadata_value "$META_DATA_ANLTCS_SOURCE_ID" "$ANLTCS_SOURCE_ID"
  fi
  # Patch $META_DATA_ANLTCS_WL_ID
  if [ -n "$ANLTCS_WL_ID" ]; then
    replace_metadata_value "$META_DATA_ANLTCS_WL_ID" "$ANLTCS_WL_ID"
  fi
  # Patch $META_DATA_ANLTCS_WL_ACCESS_KEY
  if [ -n "$ANLTCS_WL_ACCESS_KEY" ]; then
    replace_metadata_value "$META_DATA_ANLTCS_WL_ACCESS_KEY" "$ANLTCS_WL_ACCESS_KEY"
  fi

  # Return to initial directory (previous directory)
  cd - || exit

  # Build APK from working directory's files
  echolog "Building..."
  new_filename="$(basename "${file%.*}")_${WHITELABEL}_$(date +$DATETIME_FORMAT).apk"
  apktool b "$working_directory" -o "$new_filename"

  # Extract binary AndroidManifest.xml from compiled APK.
  unzip "$new_filename" "$ANDROID_MANIFEST"

  rm "$new_filename"
  cp "${file}" "$new_filename"

  # Remove AndroidManifest.xml from original APK.
  zip -d "$new_filename" "$ANDROID_MANIFEST"
  # Add extracted AndroidManifest.xml to original APK.
  zip "$new_filename" "$ANDROID_MANIFEST"
  rm "$ANDROID_MANIFEST"

  # Zipalign APK
  zipalign_apk "$new_filename"

  # Sign APK
  sign_apk "$new_filename"

  # Create "OUTPUT_DIRECTORY" and move completed apk into that directory
  echolog "Move apk to \"$OUTPUT_DIRECTORY\" directory"
  mkdir -p "$OUTPUT_DIRECTORY"
  mv "$new_filename" "$OUTPUT_DIRECTORY"

  echolog "Remove \"$working_directory\" directory..."
  rm -r "$working_directory"
  ((COUNTER++))
  echolog "################ ${COUNTER}/${FILES_COUNT} done ################"
}

##############################################################################

# Init named arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
  --whitelabel)
    declare -r WHITELABEL="$2"
    shift
    ;;
  --anltcs_source_id)
    declare -r ANLTCS_SOURCE_ID="$2"
    shift
    ;;
  --anltcs_wl_id)
    declare -r ANLTCS_WL_ID="$2"
    shift
    ;;
  --anltcs_wl_access_key)
    declare -r ANLTCS_WL_ACCESS_KEY="$2"
    shift
    ;;
  *)
    echo "Unknown param: $1"
    exit 1
    ;;
  esac
  shift
done

# Arguments validation
if [ -z "$WHITELABEL" ]; then
  echolog "${COLOR_RED}Input whitelabel name and try again.${STYLE_RESET}" "${COLOR_RED}"
  exit 1
fi

# Check if there are APKs in current directory
((FILES_COUNT = $(find . -maxdepth 1 -name "*.apk" | grep -c '^')))
if [ "$FILES_COUNT" -lt 1 ]; then
  echolog "No APK files in current directory."
  exit 1
else
  echolog "Found $FILES_COUNT APK file(s)."
fi

# Run whitelabel's resolving process
find . -maxdepth 1 -name "*.apk" -print0 |
  while IFS= read -r -d '' file; do
    resolve "${file##*/}"
  done
