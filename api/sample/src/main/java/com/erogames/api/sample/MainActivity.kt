package com.erogames.api.sample

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.erogames.api.ErogamesApi
import com.erogames.api.OnResult
import com.erogames.api.model.DataPoint
import com.erogames.api.model.PaymentInfo
import com.erogames.api.model.QuestData
import com.erogames.api.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var eroApi: ErogamesApi
    private val accessToken: String = "b36ZCMqjzbQY_KfWQgqzUzTmYcGjXbHViVRohIsbr_c"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        eroApi = ErogamesApi.getInstance(this)

        findViewById<View>(R.id.load_user).setOnClickListener { onLoadUser() }
        findViewById<View>(R.id.proceed_payment).setOnClickListener { onProceedPayment() }
        findViewById<View>(R.id.add_data_points).setOnClickListener { onAddDataPoints() }
        findViewById<View>(R.id.load_current_quest).setOnClickListener { onLoadCurrentQuest() }
    }

    private fun onLoadUser() {
        ErogamesApi.loadUser(this, accessToken, object : OnResult<User> {
            override fun onSuccess(data: User) {
                Log.d(TAG, "onLoadUser: $data")
            }

            override fun onFailure(t: Throwable?) {
                Log.w(TAG, "onLoadUser: ${t?.message}")
            }
        })
    }

    private fun onProceedPayment() {
        val paymentId = UUID.randomUUID().toString()
        val amount = 1
        ErogamesApi.proceedPayment(this, accessToken, paymentId, amount, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                Log.d(TAG, "onProceedPayment: success")
                loadPaymentInfo(paymentId)
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onProceedPayment: ${t?.message}")
            }
        })
    }

    private fun loadPaymentInfo(paymentId: String) {
        ErogamesApi.loadPaymentInfo(this, accessToken, paymentId, object : OnResult<PaymentInfo> {
            override fun onSuccess(data: PaymentInfo) {
                Log.d(TAG, "loadPaymentInfo: $data")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "loadPaymentInfo: ${t?.message}")
            }
        })
    }

    private fun onAddDataPoints() {
        val dataPoints = listOf(
            DataPoint("here", "be"),
            DataPoint("dragons", "5"),
            DataPoint("current_experience", "12500"),
        )

        ErogamesApi.addDataPoints(this, accessToken, dataPoints, object : OnResult<Void?> {
            override fun onSuccess(data: Void?) {
                Log.d(TAG, "onAddDataPoints: success")
            }

            override fun onFailure(t: Throwable?) {
                Log.d(TAG, "onAddDataPoints: ${t?.message}")
            }
        })
    }

    private fun onLoadCurrentQuest() {
        ErogamesApi.loadCurrentQuest(this, accessToken, object : OnResult<QuestData> {
            override fun onSuccess(data: QuestData) {
                Log.d(TAG, "onLoadCurrentQuest: $data")
            }

            override fun onFailure(t: Throwable?) {
                Log.e(TAG, "onLoadCurrentQuest: ${t?.message}")
            }
        })
    }

    private fun getCoroutineScope(ctx: Context): CoroutineScope = when (ctx) {
        is AppCompatActivity -> ctx.lifecycleScope
        else -> CoroutineScope(Dispatchers.Main)
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}