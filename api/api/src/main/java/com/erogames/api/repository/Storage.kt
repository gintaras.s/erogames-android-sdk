package com.erogames.api.repository

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.erogames.api.model.WhitelabelCache
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal class Storage private constructor(private val prefs: SharedPreferences) {

    fun getWhitelabelCache(): WhitelabelCache? {
        val wlCacheStr = prefs.getString(PREF_WL_CACHE, null) ?: return null
        return Json { ignoreUnknownKeys = true }.decodeFromString<WhitelabelCache>(wlCacheStr)
    }

    fun setWhitelabelCache(wlCache: WhitelabelCache) {
        val wlCacheStr = Json { ignoreUnknownKeys = true }.encodeToString(wlCache)
        prefs.edit().putString(PREF_WL_CACHE, wlCacheStr).apply()
    }

    companion object {

        @Volatile
        private var instance: Storage? = null

        private const val PREF_WL_CACHE = "com_erogames_api_pref_wl_cache"

        @JvmStatic
        fun getInstance(context: Context): Storage {
            return instance ?: synchronized(this) {
                instance ?: Storage(
                    PreferenceManager.getDefaultSharedPreferences(context)
                ).also { instance = it }
            }
        }
    }
}