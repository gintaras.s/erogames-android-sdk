package com.erogames.api.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Whitelabel(
    val slug: String,
    val name: String,
    val url: String,
)