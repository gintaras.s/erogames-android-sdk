package com.erogames.api.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class BaseStatusResp(val status: String, val code: Int? = null)