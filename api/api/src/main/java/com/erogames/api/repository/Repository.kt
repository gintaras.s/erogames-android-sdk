package com.erogames.api.repository

import android.content.Context
import android.util.Log
import com.erogames.api.R
import com.erogames.api.api.ApiService
import com.erogames.api.model.*
import com.erogames.api.util.InjectorUtil
import com.erogames.api.util.Utils

internal class Repository(
    private val whitelabelId: String,
    private val accessKey: String,
    private val apiService: ApiService,
    private val storage: Storage
) {

    private suspend fun loadWhitelabel(useCache: Boolean = true): Whitelabel {
        if (useCache) {
            val wl = storage.getWhitelabelCache()
            if (wl != null
                && wl.whitelabel.slug == whitelabelId
                && WhitelabelCache.isWlCacheValid(wl)
            ) {
                Log.i(TAG, "The whitelabel is taken from the cache.")
                return wl.whitelabel
            }
        }

        val jwtToken = apiService.getJwtToken(accessKey)
        val whitelabels = apiService.getWhitelabels(jwtToken.token).whitelabels
        val wl = whitelabels.find { it.slug == whitelabelId }
            ?: throw NullPointerException("The whitelabel '$whitelabelId' does not exist.")

        storage.setWhitelabelCache(WhitelabelCache(wl, System.currentTimeMillis() / 1000))
        return wl
    }

    suspend fun loadUser(accessToken: String): User =
        apiService.getUser(
            loadWhitelabel().url + ApiService.USER_ENDPOINT,
            "Bearer $accessToken"
        ).user

    suspend fun proceedPayment(
        accessToken: String,
        paymentId: String?,
        amount: Int?
    ): BaseStatusResp = apiService.proceedPayment(
        paymentUrl = loadWhitelabel().url + ApiService.PAYMENTS_ENDPOINT,
        accessToken = "Bearer $accessToken",
        paymentId = paymentId,
        amount = amount
    )

    suspend fun addDataPoints(accessToken: String, dataPoints: List<DataPoint>): BaseStatusResp =
        apiService.addDataPoints(
            dataPointsUrl = loadWhitelabel().url + ApiService.DATA_POINTS_ENDPOINT,
            accessToken = "Bearer $accessToken",
            dataPoints = DataPointModel(dataPoints = dataPoints)
        )

    suspend fun getCurrentQuestData(accessToken: String): QuestData =
        apiService.getCurrentQuestData(
            url = loadWhitelabel().url + ApiService.QUEST_ENDPOINT,
            accessToken = "Bearer $accessToken"
        )

    suspend fun getPaymentInfo(accessToken: String, paymentId: String): PaymentInfo =
        apiService.getPaymentInfo(
            url = loadWhitelabel().url + String.format(ApiService.PAYMENT_INFO_ENDPOINT, paymentId),
            accessToken = "Bearer $accessToken"
        )

    companion object {
        private const val TAG = "Repository"

        @Volatile
        private var instance: Repository? = null

        @JvmStatic
        fun getInstance(ctx: Context): Repository {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val appMetaData = Utils.getAppMetadata(ctx)
                    val whitelabelId = appMetaData?.getString("com_erogames_api_whitelabel_id")!!
                    val accessKey =
                        ctx.getString(R.string.com_erogames_api_access_key)

                    return Repository(
                        whitelabelId,
                        accessKey,
                        InjectorUtil.provideApiService(ctx),
                        InjectorUtil.provideStorage(ctx)
                    ).also {
                        instance = it
                    }

                }
                return instance!!
            }
        }
    }
}