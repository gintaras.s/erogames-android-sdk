# Erogames Api Library

## Integrating into the project

To add this library to the project:

1. Add repository in your project-level `build.gradle`.
```groovy
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.com/api/v4/projects/20889762/packages/maven"
        }
    }
}
```

2. Add the following dependency in your app-level `build.gradle`.
```groovy
dependencies {
    ...
    implementation "com.erogames.api:api:<x.y.z>"
}
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-android-sdk/-/tags).  
If the tag name is `api-v1.4.2` then `<x.y.z>` is `1.4.2`.

## Usage

### Load user:
```kotlin
ErogamesApi.loadUser(context, accessToken, callback)
```

### Proceed payment:
```kotlin
val paymentId = "payment_uuid"
val amount = 1
ErogamesApi.proceedPayment(context, accessToken, paymentId, amount, callback)
```

### Load current quest info:
```kotlin
ErogamesApi.loadCurrentQuest(context, accessToken, listener)
```

### Load payment info:
```kotlin
ErogamesApi.loadPaymentInfo(context, accessToken, paymentId, callback)
```
