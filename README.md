# Erogames Android SDK

...

## Erogames Auth Library

The library for user authentication via Erogames API.
[README](https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/auth)

## Erogames Analytics Library
[README](https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/analytics)

## Whitelabel Resolver Script
[README](https://gitlab.com/sky-network/erogames-android-sdk/-/tree/master/wl_resolver)
