package com.erogames.analytics.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class EventInfo(
    @PrimaryKey
    val id: String,
    val data: TrackingDataModel,
    val createdAt: Long,
)