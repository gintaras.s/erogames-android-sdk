package com.erogames.analytics.db

import android.database.Cursor
import androidx.room.*
import com.erogames.analytics.model.TrackInfo

@Dao
internal abstract class TrackInfoDao {

    @Query("SELECT * FROM trackinfo ORDER BY createdAt DESC")
    abstract fun fetchAllWithCursor(): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(trackInfo: TrackInfo): Long

    @Update
    abstract fun update(trackInfo: TrackInfo): Int

    @Query("DELETE FROM trackinfo WHERE id = :id")
    abstract fun delete(id: Long): Int
}