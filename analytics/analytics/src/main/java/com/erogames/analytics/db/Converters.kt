package com.erogames.analytics.db

import androidx.room.TypeConverter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

object Converters {

    @TypeConverter
    @JvmStatic
    fun toMap(value: String): Map<String, String> = Json { ignoreUnknownKeys = true }.decodeFromString(value)

    @TypeConverter
    @JvmStatic
    fun fromMap(data: Map<String, String>): String = Json { ignoreUnknownKeys = true }.encodeToString(data)
}