package com.erogames.analytics.util

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import com.erogames.analytics.model.WhitelabelCache

internal class Utils private constructor() {
    companion object {
        fun getAppMetadata(ctx: Context): Bundle? {
            val applicationInfo: ApplicationInfo = ctx.packageManager.getApplicationInfo(
                ctx.packageName,
                PackageManager.GET_META_DATA
            )
            return applicationInfo.metaData
        }

        fun isWlCacheValid(whitelabelCache: WhitelabelCache): Boolean {
            val currentTimeStamp = System.currentTimeMillis() / 1000
            return (whitelabelCache.createdAt + (24 * 60 * 60)) > currentTimeStamp
        }
    }
}