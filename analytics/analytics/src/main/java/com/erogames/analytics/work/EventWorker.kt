package com.erogames.analytics.work

import android.content.Context
import android.util.Log
import androidx.work.*
import com.erogames.analytics.model.EventInfo
import com.erogames.analytics.util.InjectorUtil
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

internal class EventWorker(appContext: Context, workerParams: WorkerParameters) :
    CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        Log.i(TAG, "Trying to send event data: ${inputData.keyValueMap}")
        val message: String?
        var workResult = Result.success()

        if (runAttemptCount >= RUN_ATTEMPT_COUNT_LIMIT) {
            message = "Number of attempts exceeded. The task will be canceled."
            workResult = Result.failure()
            debugResult(workResult, message)
            return workResult
        }

        val eventInfo =
            Json { ignoreUnknownKeys = true }.decodeFromString<EventInfo>(
                inputData.getString(
                    EVENT_INFO_KEY
                )!!
            )

        val repository = InjectorUtil.provideRepository(applicationContext)
        try {
            val resp = repository.sendEvent(eventInfo.data)
            message = if (resp.isSuccessful()) null else resp.status
            workResult = if (resp.isSuccessful()) Result.success() else Result.failure()
            debugResult(workResult, message)
        } catch (e: Exception) {
            workResult = Result.retry()
            debugResult(workResult, "Will try to send later.", e)
        } finally {
            return workResult
        }
    }

    private fun debugResult(
        workResult: Result,
        message: String? = null,
        tr: Throwable? = null,
    ) {
        val workResultMessage = "Send event result: $workResult"
        when (workResult) {
            Result.success() -> Log.i(TAG, workResultMessage)
            Result.retry() -> Log.w(TAG, workResultMessage)
            Result.failure() -> Log.e(TAG, workResultMessage, tr)
            else -> Log.w(TAG, workResultMessage)
        }

        message?.let {
            when (workResult) {
                Result.success() -> Log.i(TAG, it)
                Result.retry() -> Log.w(TAG, it)
                Result.failure() -> Log.e(TAG, it)
                else -> Log.w(TAG, it)
            }
        }

        tr?.let { Log.e(TAG, tr.message, tr) }
    }

    companion object {
        private const val TAG = "EventWorker"
        private const val EVENT_INFO_KEY = "event_info_key"

        private const val RUN_ATTEMPT_COUNT_LIMIT: Int = 2

        fun logEvent(ctx: Context, eventInfo: EventInfo) {
            val eventInfoStr = Json { ignoreUnknownKeys = true }.encodeToString(eventInfo)
            val inputData: Data = Data.Builder()
                .putString(EVENT_INFO_KEY, eventInfoStr)
                .build()

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val oneTimeWorkRequest = OneTimeWorkRequest.Builder(EventWorker::class.java)
                .setInputData(inputData)
                .setConstraints(constraints)
                .addTag(TAG)
                .build()

            Log.i(TAG, "The event has been added to the queue:")
            Log.i(TAG, eventInfoStr)
            WorkManager.getInstance(ctx).enqueue(oneTimeWorkRequest)
        }
    }
}