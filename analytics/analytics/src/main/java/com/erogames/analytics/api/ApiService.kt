package com.erogames.analytics.api

import android.content.Context
import com.erogames.analytics.BuildConfig
import com.erogames.analytics.R
import com.erogames.analytics.model.EventResp
import com.erogames.analytics.model.JwtTokenResp
import com.erogames.analytics.model.TrackingDataModel
import com.erogames.analytics.model.WhiteLabelsResp
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.http.*

internal interface ApiService {

    @POST
    suspend fun sendEvent(
        @Url url: String,
        @Body dataModel: TrackingDataModel
    ): EventResp

    /**
     * Returns JWT token data.
     * @param accessKey
     * @return JwtTokenResp
     */
    @FormUrlEncoded
    @POST(AUTHENTICATE_ENDPOINT)
    suspend fun getJwtToken(
        @Field("access_key") accessKey: String?,
    ): JwtTokenResp

    /**
     * Returns whitelabels data.
     * @param jwtToken
     * @return [WhiteLabelsResp]
     */
    @GET("api/v1/whitelabels")
    suspend fun getWhitelabels(@Header("Authorization") jwtToken: String): WhiteLabelsResp

    companion object {

        @Volatile
        private var instance: ApiService? = null
        internal const val TRACK_ENDPOINT: String = "api/v1/tracking"
        internal const val AUTHENTICATE_ENDPOINT: String = "api/v1/authenticate"

        @ExperimentalSerializationApi
        @JvmStatic
        fun getInstance(ctx: Context): ApiService {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val clientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
                    clientBuilder.addInterceptor(ErrorInterceptor())
                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.level = HttpLoggingInterceptor.Level.BODY
                        clientBuilder.addNetworkInterceptor(logging)
                    }

                    val contentType = "application/json".toMediaType()
                    val retrofitBuilder = Retrofit.Builder()
                        .baseUrl(ctx.getString(R.string.com_erogames_analytics_base_url))
                        .addConverterFactory(
                            Json { ignoreUnknownKeys = true }.asConverterFactory(
                                contentType
                            )
                        )
                    retrofitBuilder.client(clientBuilder.build())

                    return retrofitBuilder.build()
                        .create(ApiService::class.java).also { instance = it }
                }
                return instance!!
            }
        }
    }
}