package com.erogames.analytics.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class TrackingDataModel(
    @SerialName("type")
    val event: String,
    val category: String,
    @SerialName("client_id")
    val clientId: String,
    @SerialName("tracking_uuid")
    val trackId: String,
    @SerialName("source_slug")
    val sourceId: String,
    val details: Map<String, String>
)