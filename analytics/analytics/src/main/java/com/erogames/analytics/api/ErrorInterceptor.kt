package com.erogames.analytics.api

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject

/**
 * If a response is not successful,
 * this interceptor takes an error message from the response's body
 * and override a response message.
 */
class ErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val response = chain.proceed(request)
        val bodyResp = response.peekBody(2048).string()

        val errorMessage = when {
            response.isSuccessful.not() ->
                extractErrorMessage(bodyResp) ?: response.message
            else -> response.message
        }

        return when {
            response.isSuccessful -> response
            else -> response.newBuilder().message(errorMessage).build()
        }
    }

    private fun extractErrorMessage(string: String?): String? {
        string?.let {
            try {
                val jsonObject = JSONObject(it)

                if (jsonObject.has("message"))
                    return jsonObject.getString("message")

                if (jsonObject.has("error_description"))
                    return jsonObject.getString("error_description")

                if (jsonObject.has("errors")) {
                    val jSONArray = JSONArray(jsonObject.getString("errors"))
                    return jSONArray.join(",")
                }

                if (jsonObject.has("messages")) {
                    val jSONArray = JSONArray(jsonObject.getString("messages"))
                    return jSONArray.join(",")
                }
            } catch (e: Exception) {
                return null
            }
        }
        return null
    }
}