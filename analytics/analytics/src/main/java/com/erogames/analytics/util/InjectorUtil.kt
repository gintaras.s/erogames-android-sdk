package com.erogames.analytics.util

import android.content.Context
import com.erogames.analytics.api.ApiService
import com.erogames.analytics.db.AnalyticsDb
import com.erogames.analytics.repository.Repository
import com.erogames.analytics.repository.Storage

internal object InjectorUtil {

    @JvmStatic
    fun provideApiService(context: Context): ApiService =
        ApiService.getInstance(context.applicationContext)

    @JvmStatic
    fun provideDb(context: Context): AnalyticsDb =
        AnalyticsDb.getInstance(context.applicationContext)

    @JvmStatic
    fun provideRepository(context: Context): Repository =
        Repository.getInstance(context.applicationContext)

    @JvmStatic
    fun provideStorage(context: Context): Storage =
        Storage.getInstance(context.applicationContext)
}