# Version 1.2.4 - Nov 13, 2021
## Fixed
* The provider permission name conflict.

# Version 1.2.2 - Jun 11, 2021
## Updated
* Gradle dependencies.

# Version 1.2.1 - Jun 03, 2021
## Removed
* `kotlin-android-extensions` plugin.

# Version 1.2.0 - May 27, 2021
## Added
* Whitelabel support.

## Updated
* Android/Kotlin gradle plugins and dependencies.
* Project gradle dependencies.

## Changed
* Event's data structure.
* APIs tracking endpoint.
* Decreased the run attempt count for the event sending.
