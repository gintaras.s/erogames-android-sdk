# Version 1.4.11 - Oct 24, 2022
## Updated
* AGP, project dependencies and target/compile SDK version.

# Version 1.4.10 - Jun 11, 2021
* Decreased 'kotlinx-serialization-json' version (due to 'kotlin_module conflict' issue on older Android Gradle plugin versions).

# Version 1.4.8 - Jun 10, 2021
## Changed
* The way to set the 'whitelabel id'. Now, this value can only be set in the AndroidManifest.xml file.
## Removed
* 'com_erogames_sdk_client_id' string resource.

# Version 1.4.7 - May 31, 2021
* Minor update.

# Version 1.4.6 - May 14, 2021
## Improved
* An access token will be refreshed automatically (during API request) if it's expired.

# Version 1.4.5 - May 14, 2021
## Replaced
* JCenter repository by Maven Central.

# Version 1.4.4 - Apr 24, 2021
## Added
* An ability to get payment information.

# Version 1.4.3 - Apr 20, 2021
## Removed
* Unused string resources.
## Updated
* Sample.
* README.md.
* Project gradle dependencies.

# Version 1.4.2 - Mar 30, 2021
## Added
* New fields to QuestData: 'bestPlayers' and 'userAttempt'.