package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Whitelabel(
    val slug: String,
    val name: String,
    val url: String,
    @SerialName("logo_url")
    val logoUrl: String? = null,
    @SerialName("dark_background_logo_url")
    val darkBackgroundLogoUrl: String? = null,
    @SerialName("dark_background_logo_svg_url")
    val darkBackgroundLogoSvgUrl: String? = null,
    @SerialName("bright_background_logo_url")
    val brightBackgroundLogoUrl: String? = null,
    @SerialName("bright_background_logo_svg_url")
    val brightBackgroundLogoSvgUrl: String? = null,
    @SerialName("authorize_url")
    val authorizeUrl: String,
    @SerialName("token_url")
    val tokenUrl: String,
    @SerialName("profile_url")
    val profileUrl: String,
    @SerialName("buy_erogold_url")
    val buyErogoldUrl: Map<String, String>,
    @SerialName("support_url")
    val supportUrl: Map<String, String>,
    @SerialName("tos_url")
    val tosUrl: Map<String, String>,
)