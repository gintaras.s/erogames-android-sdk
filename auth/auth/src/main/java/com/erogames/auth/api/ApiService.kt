package com.erogames.auth.api

import android.content.Context
import com.erogames.auth.BuildConfig
import com.erogames.auth.R
import com.erogames.auth.model.*
import com.erogames.auth.util.Constants
import com.erogames.auth.util.InjectorUtil
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.http.*

internal interface ApiService {

    /**
     * Returns JWT token data.
     * @param accessKey
     * @return JwtTokenResp
     */
    @FormUrlEncoded
    @POST("api/v1/authenticate")
    suspend fun getJwtToken(
        @Field("access_key") accessKey: String?,
    ): JwtTokenResp

    @DELETE
    suspend fun logout(@Url logoutUrl: String): String

    /**
     * Returns whitelabels data.
     * @param jwtToken
     * @return [WhiteLabelsResp]
     * @see [getJwtToken]
     */
    @GET("api/v1/whitelabels")
    suspend fun getWhitelabels(@Header("Authorization") jwtToken: String): WhiteLabelsResp

    /**
     * Returns user data.
     * @param profileUrl Appropriate Whitelabel's profile URL
     * @param accessToken
     * @return [UserResp]
     */
    @GET
    suspend fun getUser(
        @Url profileUrl: String,
        @Header("Authorization") accessToken: String,
    ): UserResp

    /**
     * Registers a new user.
     * @param clientId
     * @param clientSecret
     * @param username
     * @param password
     * @param passwordConfirmation
     * @param email
     * @param checkTermsOfUse
     * @return [RegisterUserResp]
     */
    @FormUrlEncoded
    @POST("api/v1/register")
    suspend fun registerUser(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("passwordConfirmation") passwordConfirmation: String?,
        @Field("email") email: String?,
        @Field("checkTermsOfUse") checkTermsOfUse: Boolean = false,
    ): RegisterUserResp

    /**
     * Returns [Token].
     * @param tokenUrl Appropriate Whitelabel's token URL
     * @param clientId
     * @param code
     * @param grantType
     * @param redirectUri
     * @param codeVerifier
     * @return [Token]
     */
    @FormUrlEncoded
    @POST
    suspend fun getToken(
        @Url tokenUrl: String,
        @Field("client_id") clientId: String?,
        @Field("code") code: String?,
        @Field("grant_type") grantType: String? = "authorization_code",
        @Field("redirect_uri") redirectUri: String?,
        @Field("code_verifier") codeVerifier: String?,
    ): Token

    /**
     * Returns [Token].
     * @param tokenUrl Appropriate Whitelabel's token URL
     * @param clientId
     * @param grantType
     * @param login
     * @param password
     * @return [Token]
     */
    @FormUrlEncoded
    @POST
    suspend fun getTokenByPassword(
        @Url tokenUrl: String,
        @Field("client_id") clientId: String,
        @Field("grant_type") grantType: String? = "password",
        @Field("login") login: String?,
        @Field("password") password: String?,
    ): Token

    /**
     * Refresh [Token].
     * @param tokenUrl Appropriate Whitelabel's token URL
     * @param clientId
     * @param grantType
     * @param refreshToken
     * @return a new [Token]
     */
    @FormUrlEncoded
    @POST
    suspend fun refreshToken(
        @Url tokenUrl: String,
        @Field("client_id") clientId: String?,
        @Field("grant_type") grantType: String? = "refresh_token",
        @Field("refresh_token") refreshToken: String?,
    ): Token

    /**
     * Proceed payment.
     * @param paymentUrl Appropriate Whitelabel's payment URL
     * @param accessToken
     * @param paymentId
     * @param amount
     * @return [BaseStatusResp]
     */
    @FormUrlEncoded
    @POST
    suspend fun proceedPayment(
        @Url paymentUrl: String,
        @Header("Authorization") accessToken: String,
        @Field("paymentId") paymentId: String?,
        @Field("amount") amount: Int?,
    ): BaseStatusResp

    /**
     * Adds data points.
     * @param dataPointsUrl Appropriate Whitelabel's data points URL
     * @param accessToken
     * @param dataPoints [DataPointModel]
     * @return [BaseStatusResp]
     */
    @PUT
    suspend fun addDataPoints(
        @Url dataPointsUrl: String,
        @Header("Authorization") accessToken: String,
        @Body dataPoints: DataPointModel,
    ): BaseStatusResp


    /**
     * Get current quest data
     *
     * @param url
     * @param accessToken
     * @return [QuestData]
     */
    @GET
    suspend fun getCurrentQuestData(
        @Url url: String,
        @Header("Authorization") accessToken: String,
    ): QuestData

    /**
     * Get payment information
     * @return [PaymentInfo]
     */
    @GET
    suspend fun getPaymentInfo(
        @Url url: String,
        @Header("Authorization") accessToken: String,
    ): PaymentInfo

    companion object {

        /**
         * [ApiService] singleton instance.
         */
        @Volatile
        private var instance: ApiService? = null

        /**
         * Returns [ApiService] singleton instance.
         */
        @ExperimentalSerializationApi
        @JvmStatic
        fun getInstance(ctx: Context): ApiService {
            return instance ?: synchronized(this) {
                if (instance == null) {
                    val clientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
                    clientBuilder.addInterceptor(ErrorInterceptor())
                    clientBuilder.addInterceptor(Interceptor { chain ->
                        val request: Request = chain.request().newBuilder()
                            .addHeader("X-SDK-VERSION", Constants.X_SDK_VERSION)
                            .build()
                        chain.proceed(request)
                    })

                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.level = HttpLoggingInterceptor.Level.BODY
                        clientBuilder.addNetworkInterceptor(logging)
                    }

                    val contentType = "application/json".toMediaType()
                    val retrofitBuilder = Retrofit.Builder()
                        .baseUrl(ctx.getString(R.string.com_erogames_sdk_base_url))
                        .addConverterFactory(InjectorUtil.myJson.asConverterFactory(contentType))
                    retrofitBuilder.client(clientBuilder.build())

                    return retrofitBuilder.build()
                        .create(ApiService::class.java).also { instance = it }
                }
                return instance!!
            }
        }
    }
}