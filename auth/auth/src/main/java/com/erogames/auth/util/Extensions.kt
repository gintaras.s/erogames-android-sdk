package com.erogames.auth.util

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.widget.Toast
import java.security.MessageDigest
import java.util.*

/**
 * Returns SHA-256 hash of string
 */
internal fun String.sha256(): ByteArray = hash("SHA-256")

/**
 * Return hash of string.
 * @param algorithm
 */
internal fun String.hash(algorithm: String): ByteArray = MessageDigest
    .getInstance(algorithm)
    .digest(toByteArray())

/**
 * Returns localized string.
 * @param resId
 * @param locale
 */
internal fun Context.getLocalizedString(resId: Int, locale: Locale): String {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
        return getString(resId)
    }
    val configuration = Configuration(resources.configuration)
    configuration.setLocale(locale)
    return createConfigurationContext(configuration).getString(resId)
}

/**
 * Show [Toast]
 * @param message
 * @param duration
 */
internal fun Context.showToast(message: String?, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, message, duration).show()