package com.erogames.auth.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erogames.auth.repository.Repository

/**
 * A ViewModel Factory for [AuthCallbackViewModel]
 */
internal class AuthCallbackViewModelFactory(
    private val photoRepository: Repository,
    private val accessKey: String,
    private val codeVerifier: String,
    private val redirectUri: String
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(AuthCallbackViewModel::class.java)) {
            AuthCallbackViewModel(
                photoRepository,
                accessKey,
                codeVerifier,
                redirectUri
            ) as T
        } else throw IllegalArgumentException("Unknown ViewModel class")
    }
}