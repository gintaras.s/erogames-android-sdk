package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class RegisterUserResp(val message: String?) {
    fun isValid(): Boolean = "User created" == message
}