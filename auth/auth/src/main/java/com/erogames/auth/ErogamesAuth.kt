package com.erogames.auth

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.erogames.auth.model.*
import com.erogames.auth.ui.ApiIssueDialog
import com.erogames.auth.ui.AuthCallbackActivity
import com.erogames.auth.util.ApiOutdatedException
import com.erogames.auth.util.AuthUtil
import com.erogames.auth.util.Constants
import com.erogames.auth.util.InjectorUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

@Keep
object ErogamesAuth {
    private const val TAG = "ErogamesAuth"
    const val REQUEST_CODE_AUTH = 0x100
    const val REQUEST_CODE_LOGOUT = 0x101
    const val EXTRA_ERROR = "${Constants.PACKAGE_NAME_PREFIX}.extra.ERROR"
    const val EXTRA_USER = "${Constants.PACKAGE_NAME_PREFIX}.extra.USER"

    /**
     * ErogamesAuth initialization.
     *
     * @param ctx [Context]
     * @param clientId
     */
    @JvmStatic
    fun init(ctx: Context, clientId: String) {
        Log.i(TAG, "v${BuildConfig.VERSION_NAME}")

        InjectorUtil.provideAuthRepository(ctx).setClientId(clientId.trim())
        val whitelabelId = AuthUtil.getWhitelabelIdFromMeta(ctx)!!
        val repo = InjectorUtil.provideAuthRepository(ctx)
        if (whitelabelId != repo.getWhitelabel()?.slug) repo.clearLocalData()
        repo.setWhitelabelId(whitelabelId)
    }

    /**
     * Starts sign up process. The corresponding "signup" web URL will be opened in the default browser.
     * The Custom Chrome Tabs can be used if available.
     * To get authentication result, use [REQUEST_CODE_AUTH] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param locale The preferable language of the "signup" web page.
     * By default, [Locale.ENGLISH] will be used.
     */
    @JvmStatic
    fun signup(ctx: Activity, locale: Locale = Locale.ENGLISH) {
        val repo = InjectorUtil.provideAuthRepository(ctx)
        repo.setLocale(locale)
        AuthCallbackActivity.signupAction(ctx)
    }

    /**
     * Starts sign in process. The corresponding "sign in" web URL will be opened in the default browser.
     * The Custom Chrome Tabs can be used if available.
     * To get authentication result, use [REQUEST_CODE_AUTH] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param locale The preferable language of the "signup" web page.
     * By default, [Locale.ENGLISH] will be used.
     */
    @JvmStatic
    fun login(ctx: Activity, locale: Locale = Locale.ENGLISH) {
        val repo = InjectorUtil.provideAuthRepository(ctx)
        repo.setLocale(locale)
        AuthCallbackActivity.loginAction(ctx)
    }

    /**
     * Login by username(email)/password.
     * To get a user use [getUser] in [OnResult.onSuccess] method.
     */
    @JvmStatic
    fun loginByPassword(
        ctx: Context,
        username: String?,
        password: String?,
        listener: OnResult<Void?>,
    ) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val user: User? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                val clientId = repo.getClientId()
                val accessKey = AuthUtil.getAccessKey(ctx)
                val whitelabelId = repo.getWhitelabelId()

                try {
                    val jwtToken = repo.loadJwtToken(accessKey)
                    val whitelabel = repo.loadWhitelabel(jwtToken, whitelabelId)
                    repo.loadTokenByPassword(whitelabel.tokenUrl, clientId, username, password)
                    repo.loadUser(whitelabel.profileUrl)
                } catch (e: Exception) {
                    Log.e(TAG, "loginByPassword", e)
                    t = e
                    null
                }
            }
            when {
                user != null -> listener.onSuccess(null)
                else -> {
                    if (t is ApiOutdatedException) ApiIssueDialog.show(ctx)
                    listener.onFailure(t)
                }
            }
        }
    }

    /**
     * Register of a new user
     * To "log in" use  [login] or [loginByPassword] in [OnResult.onSuccess] method.
     */
    @JvmStatic
    fun registerUser(
        ctx: Context,
        clientSecret: String,
        username: String?,
        password: String?,
        email: String?,
        checkTermsOfUse: Boolean,
        listener: OnResult<Void?>,
    ) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val registerUserResp: RegisterUserResp? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                val clientId = repo.getClientId()
                try {
                    InjectorUtil.provideAuthRepository(ctx).registerUser(
                        clientId, clientSecret, username, password, email, checkTermsOfUse
                    )
                } catch (e: Exception) {
                    Log.e(TAG, "registerUser", e)
                    t = e
                    null
                }
            }
            when {
                registerUserResp?.isValid() ?: false -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Clear all local authentication data.
     * If [webLogout] is true then the corresponding "log out" web URL will be opened in the default browser.
     * In this case, the user will also be logged out from the web site.
     * To get log out result, use [REQUEST_CODE_LOGOUT] request code in overridden [Activity.onActivityResult] method.
     *
     * @param ctx [Activity]
     * @param webLogout Set to true for log out from the web site as well.
     */
    @JvmStatic
    @JvmOverloads
    fun logout(ctx: Activity, webLogout: Boolean = false) {
        AuthCallbackActivity.logoutAction(ctx, webLogout)
    }

    /**
     * Returns the current user data.
     *
     * @param ctx [Context]
     * @return The current user data, or null if the user is not logged in.
     */
    @JvmStatic
    fun getUser(ctx: Context): User? =
        InjectorUtil.provideAuthRepository(ctx).getUser()

    /**
     * Refreshes the current user data.
     * To get the refreshed user data use {@link #getUser(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun reloadUser(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val user: User? = withContext(Dispatchers.IO) {
                try {
                    InjectorUtil.provideAuthRepository(ctx).reloadUser()
                } catch (e: Exception) {
                    Log.e(TAG, "reloadUser", e)
                    t = e
                    null
                }
            }
            when {
                user != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Returns the current token.
     *
     * @param ctx [Context]
     * @return The the current token, or null if the user is not logged in.
     */
    @JvmStatic
    fun getToken(ctx: Context): Token? =
        InjectorUtil.provideAuthRepository(ctx).getToken()

    /**
     * Refreshes the current token. For example, can be used if the token is expired.
     * To get the refreshed token use {@link #getToken(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun refreshToken(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val token: Token? = withContext(Dispatchers.IO) {
                try {
                    InjectorUtil.provideAuthRepository(ctx).refreshToken()
                } catch (e: Exception) {
                    Log.e(TAG, "refreshToken", e)
                    t = e
                    null
                }
            }
            when {
                token != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Returns the whitelabel info.
     *
     * @param ctx [Context]
     * @return The the current [Whitelabel], or null if the user is not logged in.
     */
    @JvmStatic
    fun getWhitelabel(ctx: Context): Whitelabel? =
        InjectorUtil.provideAuthRepository(ctx).getWhitelabel()

    /**
     * Load the whitelabel. For example, can be used if the user is not logged in.
     * To get the local whitelabel use {@link #getWhitelabel(Context)}
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun loadWhitelabel(ctx: Context, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val wl: Whitelabel? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                val accessKey = AuthUtil.getAccessKey(ctx)
                val whitelabelId = repo.getWhitelabelId()

                try {
                    val jwtToken = repo.loadJwtToken(accessKey)
                    repo.loadWhitelabel(jwtToken, whitelabelId)
                } catch (e: Exception) {
                    Log.e(TAG, "loadWhitelabel", e)
                    t = e
                    null
                }
            }
            when {
                wl != null -> listener.onSuccess(null)
                else -> {
                    if (t is ApiOutdatedException) ApiIssueDialog.show(ctx)
                    listener.onFailure(t)
                }
            }
        }
    }

    /**
     * Proceed a new payment.
     *
     * @param ctx [Context]
     * @param paymentId
     * @param amount, the amount in Erogold
     * @param listener [OnResult]
     */
    @JvmStatic
    fun proceedPayment(ctx: Context, paymentId: String?, amount: Int?, listener: OnResult<Void?>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val baseStatusResp: BaseStatusResp? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                try {
                    repo.proceedPayment(paymentId, amount)
                } catch (e: Exception) {
                    Log.e(TAG, "proceedPayment", e)
                    t = e
                    null
                }
            }
            when {
                baseStatusResp != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Create/update user data points.
     *
     * Sending new data points will create a new entries. Sending a data point that was
     * already created before will update the previous values.
     *
     * @param ctx [Context]
     * @param dataPoints, [DataPoint] list
     * @param listener The [OnResult] is used to check the status of the operation.
     */
    @JvmStatic
    @Deprecated("Will be removed.")
    fun addDataPoints(
        ctx: Context,
        dataPoints: List<DataPoint>,
        listener: OnResult<Void?>,
    ) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val baseStatusResp: BaseStatusResp? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                try {
                    repo.addDataPoints(dataPoints)
                } catch (e: Exception) {
                    Log.e(TAG, "addDataPointCollection", e)
                    t = e
                    null
                }
            }
            when {
                baseStatusResp != null -> listener.onSuccess(null)
                else -> listener.onFailure(t)
            }
        }
    }

    /**
     * Loads current quest info
     *
     * Load key quest statistics and fields such as quest title and
     * description. The data returned also has information about the current ranking of
     * user's clan (user is loaded based on used auth token) and also current user's
     * individual contribution to the clan score. This endpoint allows implementing in-game
     * quest status widgets for more immersive gameplay.
     *
     * @param ctx [Context]
     * @param listener [OnResult]
     */
    @JvmStatic
    fun loadCurrentQuest(ctx: Context, listener: OnResult<QuestData>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val questData: QuestData? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                try {
                    repo.getCurrentQuestData()
                } catch (e: Exception) {
                    Log.e(TAG, "loadCurrentQuest", e)
                    t = e
                    null
                }
            }
            when {
                questData != null -> listener.onSuccess(questData)
                else -> listener.onFailure(t)
            }
        }
    }

    @JvmStatic
    fun loadPaymentInfo(ctx: Context, paymentId: String, listener: OnResult<PaymentInfo>) {
        getCoroutineScope(ctx).launch {
            var t: Throwable? = null
            val paymentInfo: PaymentInfo? = withContext(Dispatchers.IO) {
                val repo = InjectorUtil.provideAuthRepository(ctx)
                try {
                    repo.getPaymentInfo(paymentId)
                } catch (e: Exception) {
                    Log.e(TAG, "loadPaymentInfo", e)
                    t = e
                    null
                }
            }
            when {
                paymentInfo != null -> listener.onSuccess(paymentInfo)
                else -> listener.onFailure(t)
            }
        }
    }

    private fun getCoroutineScope(ctx: Context): CoroutineScope = when (ctx) {
        is AppCompatActivity -> ctx.lifecycleScope
        else -> CoroutineScope(Dispatchers.Main)
    }
}

interface OnResult<T> {
    fun onSuccess(data: T)
    fun onFailure(t: Throwable?)
}