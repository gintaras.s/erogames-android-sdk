package com.erogames.auth.util

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import com.erogames.auth.R
import java.util.*

internal object AuthUtil {
    private const val WEB_LOGOUT_URL_FORMAT: String = "%s/logout?token=%s&redirect_uri=%s"
    private const val AUTH_URL_FORMAT: String = "%s%s" +
            "code_challenge=%s" +
            "&redirect_uri=%s" +
            "&client_id=%s" +
            "&locale=%s" +
            "&force_registration=%s" +
            "&code_challenge_method=S256" +
            "&response_type=code" +
            "&disclaimer=none"

    /**
     * Build authentication URL.
     * @see AUTH_URL_FORMAT
     * @param authorizeUrl
     * @param codeChallenge
     * @param redirectUri
     * @param clientId
     * @param locale
     * @param forceRegistration If true, log in page URL will be generated.
     * Otherwise sign up page URL will generated.
     */
    fun buildAuthUrl(
        authorizeUrl: String,
        codeChallenge: String,
        redirectUri: String,
        clientId: String,
        locale: Locale,
        forceRegistration: String,
    ): String = String.format(
        AUTH_URL_FORMAT,
        authorizeUrl,
        if (authorizeUrl.contains("?")) "&" else "?",
        codeChallenge,
        redirectUri,
        clientId,
        locale,
        forceRegistration
    )

    /**
     * Build web log out URL.
     * @see WEB_LOGOUT_URL_FORMAT
     * @param baseUrl
     * @param token
     * @param redirectUri
     */
    fun buildWebLogoutUrl(baseUrl: String, token: String, redirectUri: String): String =
        String.format(
            WEB_LOGOUT_URL_FORMAT,
            baseUrl,
            token,
            redirectUri
        )

    /**
     * Builds redirect URI. Used for a deep linking purpose.
     * @param ctx
     */
    fun buildRedirectUri(ctx: Context): String {
        val redirectHost = ctx.getString(R.string.com_erogames_sdk_redirect_host)
        val redirectScheme = ctx.packageName
        return "$redirectScheme://$redirectHost"
    }

    /**
     * Returns access key.
     * @param ctx
     */
    fun getAccessKey(ctx: Context): String =
        ctx.getString(R.string.com_erogames_sdk_access_key)

    fun getWhitelabelIdFromMeta(ctx: Context): String? {
        val applicationInfo: ApplicationInfo = ctx.packageManager.getApplicationInfo(
            ctx.packageName,
            PackageManager.GET_META_DATA
        )
        return applicationInfo.metaData?.getString("erogames_auth_whitelabel_id")
    }
}