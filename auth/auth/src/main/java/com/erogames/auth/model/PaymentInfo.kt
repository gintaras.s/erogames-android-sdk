package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class PaymentInfo(
    @SerialName("payment_id")
    val paymentId: String,
    @SerialName("debit_amount")
    val debitAmount: Int
)