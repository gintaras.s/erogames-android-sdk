package com.erogames.auth.util

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.erogames.auth.R

class Utils private constructor() {

    companion object {
        private const val TAG = "Utils"

        /**
         * Launch url in a Custom Tabs Activity.
         */
        fun launchUrl(ctx: Context, url: String) {
            Log.d(TAG, "Launch URL: $url")
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
            addSomeBeautyToCustomTab(ctx, builder)
            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(ctx, Uri.parse(url))
        }

        /**
         * Adds custom color, animation, etc to CustomTabsIntent.
         */
        private fun addSomeBeautyToCustomTab(ctx: Context, builder: CustomTabsIntent.Builder) {
            val colorSchemeParams = CustomTabColorSchemeParams.Builder()
                .setToolbarColor(ContextCompat.getColor(ctx,
                    R.color.com_erogames_sdk_color_toolbar))
                .build()

            builder.setDefaultColorSchemeParams(colorSchemeParams)
            builder.setShareState(CustomTabsIntent.SHARE_STATE_OFF)
            builder.setStartAnimations(
                ctx,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            builder.setExitAnimations(
                ctx,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }
}