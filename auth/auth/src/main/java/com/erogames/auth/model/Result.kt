package com.erogames.auth.model

import androidx.annotation.Keep

@Keep
internal sealed class Result<T>(
    val data: T? = null,
    val error: Throwable? = null
) {
    class Success<T>(data: T? = null) : Result<T>(data)
    class Loading<T>(data: T? = null) : Result<T>(data)
    class Error<T>(error: Throwable, data: T? = null) : Result<T>(data, error)
}