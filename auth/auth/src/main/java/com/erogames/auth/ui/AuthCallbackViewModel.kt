package com.erogames.auth.ui

import androidx.lifecycle.*
import com.erogames.auth.model.Result
import com.erogames.auth.model.Whitelabel
import com.erogames.auth.repository.Repository
import com.erogames.auth.util.AuthUtil
import com.erogames.auth.util.Pkce
import com.erogames.auth.util.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import java.util.*

internal class AuthCallbackViewModel(
    private val repository: Repository,
    private val accessKey: String,
    private val codeVerifier: String,
    private val redirectUri: String
) : ViewModel() {

    private val grantType: String = "authorization_code"

    private val _loadWhitelabel: SingleLiveEvent<Unit> = SingleLiveEvent()
    val onWhitelabel: LiveData<Result<Whitelabel>> = _loadWhitelabel.switchMap {
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Result.Loading())
            try {
                val jwtToken = repository.loadJwtToken(accessKey)
                val whitelabel = repository.loadWhitelabel(jwtToken, repository.getWhitelabelId())
                emit(Result.Success(whitelabel))
            } catch (exception: Exception) {
                emit(Result.Error(error = exception))
            }
        }
    }

    private val code: SingleLiveEvent<String> = SingleLiveEvent()
    val onAuth: LiveData<Result<Unit>> = code.switchMap {
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Result.Loading())
            try {
                repository.loadToken(it!!, grantType, redirectUri, codeVerifier)
                repository.loadUser()
                emit(Result.Success())
            } catch (exception: Exception) {
                emit(Result.Error(error = exception))
            }
        }
    }

    private val _onLogout: SingleLiveEvent<String?> = SingleLiveEvent()
    val onLogout = _onLogout.switchMap { apiLogoutUrl ->
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Result.Loading())
            if (apiLogoutUrl == null) {
                emit(Result.Success(true))
                return@liveData
            }
            try {
                repository.logout(apiLogoutUrl)
                emit(Result.Success(true))
            } catch (e: Exception) {
                emit(Result.Error(error = e))
            }
        }
    }

    /**
     * Set code and proceed authentication flow
     */
    fun setCode(code: String) = this.code.postValue(code)

    /**
     * Build authentication URL based on appropriate [Whitelabel]
     */
    fun buildAuthUrl(forceRegistration: Boolean): String {
        val whitelabel = repository.getWhitelabel()
        val authorizeUrl = whitelabel?.authorizeUrl!!
        val codeChallenge = Pkce.generateCodeChallenge(Pkce.getCodeVerifier())
        val clientId = repository.getClientId()
        val locale = repository.getLocale()

        return AuthUtil.buildAuthUrl(
            authorizeUrl, codeChallenge, redirectUri, clientId, locale, forceRegistration.toString()
        )
    }

    /**
     * Build log out URL based on appropriate [Whitelabel]
     */
    fun buildWebLogoutUrl(): String? {
        val whitelabel = getWhitelabel()
        val accessToken = repository.getToken()?.accessToken
        if (whitelabel != null && accessToken != null) {
            return AuthUtil.buildWebLogoutUrl(
                whitelabel.url, accessToken, redirectUri
            )
        }
        return null
    }

    /**
     * Load [Whitelabel] trigger
     */
    fun loadWhitelabel() = _loadWhitelabel.call()

    /**
     * Returns locally stored [Whitelabel]
     */
    fun getWhitelabel(): Whitelabel? = repository.getWhitelabel()

    /**
     * Returns preferred [Locale]
     */
    fun getLocale(): Locale = repository.getLocale()

    /**
     * Log out trigger
     */
    fun logout() {
        _onLogout.value = null
    }

    /**
     * Clear all local data
     */
    fun clearLocalData() = repository.clearLocalData()
}