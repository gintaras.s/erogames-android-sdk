package com.erogames.auth.model

import androidx.annotation.Keep
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
internal data class DataPointModel(@SerialName("data_points") val dataPoints: List<DataPoint>)