package com.erogames.auth.util

import android.content.Context
import com.erogames.auth.api.ApiService
import com.erogames.auth.repository.Repository
import com.erogames.auth.repository.Storage
import kotlinx.serialization.json.Json

/**
 * Simple injector.
 */
internal object InjectorUtil {
    val myJson: Json by lazy { Json { ignoreUnknownKeys = true } }

    @JvmStatic
    fun provideApiService(context: Context): ApiService =
        ApiService.getInstance(context.applicationContext)

    @JvmStatic
    fun provideAuthRepository(context: Context): Repository =
        Repository.getInstance(context.applicationContext)

    @JvmStatic
    fun provideStorage(context: Context): Storage =
        Storage.getInstance(context.applicationContext)
}