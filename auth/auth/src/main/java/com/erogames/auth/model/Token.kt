package com.erogames.auth.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Parcelize
@Serializable
data class Token(
    @SerialName("access_token")
    val accessToken: String,
    @SerialName("refresh_token")
    val refreshToken: String,
    @SerialName("token_type")
    val tokenType: String,
    val scope: String,
    @SerialName("expires_in")
    val expiresIn: Int,
    @SerialName("created_at")
    val createdAt: Int
) : Parcelable {

    fun isExpired(): Boolean {
        val timestamp = System.currentTimeMillis() / 1000
        return timestamp > createdAt + expiresIn
    }
}