package com.erogames.auth.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Parcelize
@Serializable
data class User(
    val id: Int,
    val email: String? = null,
    val username: String,
    @SerialName("avatar_url")
    val avatarUrl: String? = null,
    val language: String? = null,
    val balance: Int
) : Parcelable