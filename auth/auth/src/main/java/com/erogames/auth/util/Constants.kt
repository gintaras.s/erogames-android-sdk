package com.erogames.auth.util

internal interface Constants {
    companion object {
        const val API_MIN_VERSION: String = "1.0"
        const val X_SDK_VERSION: String = "1.1"

        const val PACKAGE_NAME_PREFIX = "com.erogames.android.auth"
        val SUPPORTED_LANGUAGES: List<String> = listOf("en", "fr", "zh", "ja", "es")

        const val QUEST_ENDPOINT = "/api/v1/quests/current"
        const val PAYMENT_INFO_ENDPOINT = "/api/v1/payments/%s"
    }
}
