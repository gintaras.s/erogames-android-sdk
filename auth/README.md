# Erogames Auth Library

The library for user authentication via Erogames API.

## Integrating into the project

To add this library to the project:

1. Add repository in your project-level `build.gradle`.
```groovy
allprojects {
    repositories {
        ...
        maven {
            url "https://gitlab.com/api/v4/projects/20889762/packages/maven"
        }
    }
}
```

2. Add the following dependency in your app-level `build.gradle`.
```groovy
dependencies {
    ...
    implementation "com.erogames.auth:auth:<x.y.z>"
}
```
The latest version can be taken on the [tags page](https://gitlab.com/sky-network/erogames-android-sdk/-/tags).  
If the tag name is `auth-v1.4.2` then `<x.y.z>` is `1.4.2`.

3. Initialize the Erogames Auth before use:
```kotlin
ErogamesAuth.init(context, your_client_id)
```

## Usage

### Log in:
```kotlin
ErogamesAuth.login(context)
```
To get the authentication result, override `onActivityResult` method of your Activity:
```kotlin
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK) {
        when (requestCode) {
            ErogamesAuth.REQUEST_CODE_AUTH -> {
                // Get error message in case of unsuccessful authentication
                val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)
                if (authError == null) {
                    val user = ErogamesAuth.getUser(this)
                } else {
                    // show error
                }
            }
            ErogamesAuth.REQUEST_CODE_LOGOUT -> Unit
        }
    }
}
```

### Log in with a username(email)/password:
```kotlin
ErogamesAuth.loginByPassword(context, username, password, listener)
```

### Sign up:
```kotlin
ErogamesAuth.signup(context)
```
To get the authentication result, override `onActivityResult` method of your Activity:
```kotlin
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK) {
        when (requestCode) {
            ErogamesAuth.REQUEST_CODE_AUTH -> {
                // Get error message in case of unsuccessful authentication
                val authError = data?.getStringExtra(ErogamesAuth.EXTRA_ERROR)
                if (authError == null) {
                    val user = ErogamesAuth.getUser(this)
                } else {
                    // show error
                }
            }
            ErogamesAuth.REQUEST_CODE_LOGOUT -> Unit
        }
    }
}
```

### Whitelabel info (base URL, logo URL, "buy Erogold" URL, authorize URL, etc.):
```kotlin
ErogamesAuth.getWhitelabel(context)
```
In case a user is not logged in yet but you need some of those Whitelabel info
(or ErogamesAuth.getWhitelabel(context) returns null):
```kotlin
ErogamesAuth.loadWhitelabel(context)
```

### Register of a new user:
```kotlin
ErogamesAuth.registerUser(context, clientSecret, username, password, email, checkTermsOfUse, listener)
```

### Log out case 1:
```kotlin
// The local data only will be cleaned
ErogamesAuth.logout(context)
```

### Log out case 2:
```kotlin
// The user will log out of the web site and
// the local data will be cleaned as well
ErogamesAuth.logout(context, true)
```

### Get current user:
```kotlin
ErogamesAuth.getUser(context)
```

### Reload user:
```kotlin
ErogamesAuth.reloadUser(context, listener)
```

### Get token:
```kotlin
ErogamesAuth.getToken(context)
```

### Refresh token:
```kotlin
ErogamesAuth.refreshToken(context, listener)
```

### Proceed payment:
```kotlin
val paymentId = "payment_uuid"
val amount = 1
ErogamesAuth.proceedPayment(context, paymentId, amount, listener)
```

### Add data points:
```kotlin
val dataPoints = listOf(
    DataPoint("here", "be"),
    DataPoint("dragons", "5"),
    DataPoint("current_experience", "12500"),
)
ErogamesAuth.addDataPoints(context, dataPoints, listener)
```

### Load current quest info:
```kotlin
ErogamesAuth.loadCurrentQuest(context, listener)
```

### Load payment information:
```kotlin
ErogamesAuth.loadPaymentInfo(context, "payment_uuid", listener)
```
